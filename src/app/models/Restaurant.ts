export class Restaurant{
  name : string = '';
  description : string = '';
  address : string = '';
  latitude : number;
  longitude : number;
  photo_url : string;
  photo_dummy : string = "assets/images/restaurant-thumbnail.svg";
  photoBase64 : string;
}
