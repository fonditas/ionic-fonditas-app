export class Product {
  id : number;
  name : string = '';
  description : string = '';
  category : string = '';
  price : number;
  photo_url : string;
  photo_dummy : string = "assets/images/food.jpg";
  photoBase64 : string;
  editButton : boolean = false;
  removeButton : boolean = false;
}
