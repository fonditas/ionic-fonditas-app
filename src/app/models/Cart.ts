export class Cart {

  starters : any [] = [];
  midDish : any [] = [];
  mainDish : any [] = [];
  desserts : any [] = [];
  drinks : any [] = [];
  products : any [] = [];
  restaurantId : number;
  total : number;
  numberOfItems : number;
  paypalId : string;
  hourOfArrive : Date;

  constructor() {
    this.total = 0;
    this.paypalId = "12390182331PHYX";
    this.hourOfArrive = null;
  }

  addItem( item : any ) {

    let collection = this.getCollection(item);

    if ( collection.length == 0 ) {

      collection.push(item);
      collection[0].quantity++;

    } else {

      let contains = false;
      for ( let it of collection ) {
        if ( it.id == item.id ) {
          it.quantity++;
          contains = true;
          break;
        }
      }

      if ( !contains ) {
        item.quantity++;
        collection.push(item);
      }

    }

    this.total += item.price;
    this.numberOfItems++;
    console.log(collection);

  }

  removeItem( item : any ){
    let collection = this.getCollection(item);
    let index = 0;
    for ( let it of collection ) {

      if ( it.id = item.id ) {

        if ( it.quantity > 1 ) {
          it.quantity--;
        } else {
          item.quantity = 0;
          collection.splice(index, 1);
        }
        this.total -= item.price;
        break;

      }
      index++;

    }
    console.log(collection);
  }

  private getCollection( item : any ) {

    switch( item.category ) {
      case 'Entradas': {
        return this.starters;
      }
      case 'Intermedios': {
        return this.midDish;
      }
      case 'Plato Fuerte': {
        return this.mainDish;
      }
      case 'Bebidas': {
        return this.drinks;
      }
      case 'Postres': {
        return this.desserts;
      }
    }

  }

  mergeAllProducts(){
    this.products = [];
    this.products = this.products.concat( this.starters );
    this.products = this.products.concat( this.midDish );
    this.products = this.products.concat( this.mainDish );
    this.products = this.products.concat( this.desserts );
    this.products = this.products.concat( this.drinks );
  }

}
