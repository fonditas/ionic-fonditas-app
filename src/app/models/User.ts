export class User{
  id : number;
  email : string = '';
  password : string = '';
  name : string = '';
  last_name : string = '';
  photo_dummy : string = "assets/images/profile-placeholder.png";
  photoBase64 : string;
}
