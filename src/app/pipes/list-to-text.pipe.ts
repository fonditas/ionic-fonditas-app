import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LinkToTextPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'listToText',
})
export class LisToTextPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any, ...args) {

    if( value.length == 0 ){
      return `No hay platillos por el momento`;
    }

    let text = ``;

    for ( var i = 0; i<value.length; i++ ){
      let item = (value[i].sold_out)? value[i].name.strike() : value[i].name;
      if ( i == value.length-1){
        text+=`${item}`;
      }else if ( i == value.length-2){
        text+=`${item} y `;
      }else{
        text+=`${item}, `;
      }
    }
    return text;

  }

  capitalize(string) {
    var splitStr = string.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

}
