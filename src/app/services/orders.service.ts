import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";
import { Cart } from "../models/Cart";

@Injectable()
export class OrdersService {

  cart : Cart;

  constructor( private httpService : HttpService) { }

  public order ( data : any ) {
    return this.httpService.makePost('order', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().order;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    );
  }

}
