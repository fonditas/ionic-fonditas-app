import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { LoadingController, Loading } from "ionic-angular";
import { HttpService } from "./http-service.service";
import { User } from "../models/User";

@Injectable()
export class AuthenticationService {

  public token : string = '';
  public user : User;

  constructor( private httpService : HttpService) { }

  public authenticate( data : any ){
    return this.httpService.makePost('authenticate', data, false).map(
      res => {
        let jsonResponse = res.json();
        console.log(jsonResponse.owner);
        localStorage.setItem('owner', jsonResponse.owner);
        return jsonResponse.token;
      },
      error => {
        return error.json();
      }
    );
  }

  public register( data : any ){
    return this.httpService.makePost('register', data).map(
      res => {
        localStorage.setItem('owner', "false");
        return res.json().token;
      },
      error => {
        return error.json();
      }
    );
  }

  public registerWithRestaurant( data : any ) {
    return this.httpService.makePost('register-with-restaurant', data).map(
      res => {
        localStorage.setItem('owner', "true");
        return res.json();
      },
      error => {
        return error.json();
      }
    );
  }

  public refreshUser() {
    this.user = new User();
  }

  public startPreservingUser( user : User ) {
    this.user = user;
  }

  public getPreservedUser() {
    return this.user;
  }

  public isOwner() {
    return JSON.parse( localStorage.getItem('owner') );
  }

}
