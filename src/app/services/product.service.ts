import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";

@Injectable()
export class ProductsService {

  constructor( private httpService : HttpService) { }

  public store ( data : any ) {
    return this.httpService.makePost('products', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().product;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    );
  }

  public update ( data : any ) {
    return this.httpService.makePut('products', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().product;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error.json());
      }
    );
  }

  public delete ( data : any ) {
    return this.httpService.makePut('delete-product', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json();
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error.json());
      }
    );
  }

  public productsActive( data : any ) {
    return this.httpService.makePut('products-active', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json();
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error.json());
      }
    );
  }

}
