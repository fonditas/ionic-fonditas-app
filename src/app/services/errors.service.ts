import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

@Injectable()
export class ErrorsService {

  constructor( private alertController : AlertController ) { }

  public showErrors( errors : any ) {
    var errorsString = '<ul class="align-left-errors">';

    for ( var i = 0; i < errors.length; i++){
      errorsString += `<li>${errors[i]}</li>`
    }

    errorsString += '</ul>';

    let alert = this.alertController.create({
      title: '¡Hay errores en tus datos!',
      subTitle: errorsString,
      buttons: ['Cerrar']
    });
    alert.present();
  }


}
