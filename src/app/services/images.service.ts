import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class ImagesService {

  constructor( private alertCtrl : AlertController, private loadingCtrl : LoadingController) { }

  public upload(image : string ){

    let storageRef = firebase.storage().ref();
    const filename = Math.floor(Date.now() / 1000);
    const imageRef = storageRef.child(`images/${filename}.jpg`);

    console.log("haciendo upload");
    return imageRef.putString(image, firebase.storage.StringFormat.DATA_URL).then((snapshot) => {

      return snapshot.downloadURL;

    });
  }

}
