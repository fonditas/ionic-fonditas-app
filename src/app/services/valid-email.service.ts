import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";

@Injectable()
export class ValidEmailService {

  constructor( private httpService : HttpService) { }

  public validateEmail(email : string) : boolean{
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(email);
  }

  public checkAvailability(data : any){
    return this.httpService.makePost('checkAvailability', data).map(
      res => {
        return res.json().available;
      },
      error => {
        console.log(error.json());
      }
    );
  }

}
