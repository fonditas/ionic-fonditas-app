import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';

@Injectable()
export class HttpService {

  constructor( private http : Http) { }

  /*
  * Until we have a server, this will be the URL
  */
  //private url = 'http://localhost:8000/api/';
  private url = 'http://159.203.184.56/api/';

  public makePost( endpoint : string, data : any, authenticated : boolean = false ) {
    return this.http.post( `${this.url}${endpoint}`, data, { headers : this.getHeaders(authenticated) } );
  }

  public makePut( endpoint : string, data : any, authenticated : boolean = false ) {
    return this.http.put( `${this.url}${endpoint}`, data, { headers : this.getHeaders(authenticated) } );
  }

  public makeGet( endpoint : string, authenticated : boolean = false ) {
    return this.http.get( `${this.url}${endpoint}`, { headers : this.getHeaders(authenticated) } );
  }

  private getHeaders(authenticated : boolean){
    let headers = new Headers();
    let token = localStorage.getItem('token');
    headers.append( 'Content-Type', 'application/json' );
    if ( authenticated ) {
      headers.append( 'Authorization', token);
    }
    return headers;
  }

}
