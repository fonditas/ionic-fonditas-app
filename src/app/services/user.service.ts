import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";

@Injectable()
export class UserService {

  constructor( private httpService : HttpService) { }

  public orders () {
    return this.httpService.makeGet('my-orders', true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().orders;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    );
  }

  public myRestaurant() {
    return this.httpService.makeGet('my-restaurant', true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().restaurant;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    );
  }

  public myRestaurantOpenClose() {
    let data = {};
    return this.httpService.makePut('my-restaurant-open-close', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().restaurant;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    );
  }

}
