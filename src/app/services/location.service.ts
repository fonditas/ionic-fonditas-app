import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";
import { Geolocation } from '@ionic-native/geolocation';
import { Location } from '../models/Location';

@Injectable()
export class LocationService {

  public firstLoad : boolean = true;
  location : Location = new Location();

  constructor( private geolocation : Geolocation ) { }

  getLocalization() {
    return this.geolocation.getCurrentPosition();
  }

  getLocalizationObject() {
    return this.location;
  }

  setLocalizationObject( location : Location ) {
    this.location = location;
  }

  resetLocationObject() {
    this.location = new Location();
    this.firstLoad = true;
  }

}
