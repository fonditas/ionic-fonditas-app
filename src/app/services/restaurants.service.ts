import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from "./http-service.service";

@Injectable()
export class RestaurantsService {

  constructor( private httpService : HttpService) { }

  public restaurants () {
    return this.httpService.makeGet('restaurants', true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().restaurants;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error);
        return error.json();
      }
    );
  }

  public favourites () {
    return this.httpService.makeGet('my-favourites', true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().restaurants;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error);
        return error.json();
      }
    );
  }

  public addFavourite ( data : any ) {
    return this.httpService.makePost('favourite', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().favourites;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error);
        return error.json();
      }
    );
  }

  public deleteFavourite ( data : any ) {
    return this.httpService.makePost('favourite-delete', data, true).map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res.json().restaurants;
      },
      error => {
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        console.log(error);
        return error.json();
      }
    );
  }

}
