import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

import firebase from 'firebase';

import { RegistrationPage } from '../pages/registration/registration';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = RegistrationPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      const firebaseConfig = {
        apiKey: "AIzaSyDRaAIVbKPyXaSMQjtnSBYo9DhYIdp7_OM",
        authDomain: "fonditas-2.firebaseapp.com",
        databaseURL: "https://fonditas-2.firebaseio.com",
        storageBucket: "fonditas-2.appspot.com",
        messagingSenderId: "fonditas-2.appspot.com"
      };

      firebase.initializeApp(firebaseConfig);

    });
  }
}
