import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

///////////// Services /////////////
/*
* Each time you create a new service, add it on the providers Array
*/
import { HttpService } from '../app/services/http-service.service';
import { AuthenticationService } from '../app/services/authentication.service';
import { ValidEmailService } from '../app/services/valid-email.service';
import { ImagesService } from '../app/services/images.service';
import { RestaurantsService } from '../app/services/restaurants.service';
import { OrdersService } from '../app/services/orders.service';
import { UserService } from '../app/services/user.service';
import { LocationService } from '../app/services/location.service';
import { ErrorsService } from '../app/services/errors.service';
import { ProductsService } from '../app/services/product.service';

///////////// Registration Pages /////////////
/*
* Each time you create a new page, add it on the declarations array
* and also on the entryComponents array
*/
import { RegistrationPage } from '../pages/registration/registration';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { SelectLocationPage } from '../pages/select-location/select-location';
import { RestaurantRegistrationPage } from '../pages/restaurant-registration/restaurant-registration';


///////////// Restaurants Pages /////////////
/*
* Each time you create a new page, add it on the declarations array
* and also on the entryComponents array
*/
import { FavouritesPage } from  '../pages/favourites/favourites';
import { SearchRestaurantsPage } from '../pages/search-restaurants/search-restaurants';


///////////// Order Pages /////////////
/*
* Each time you create a new page, add it on the declarations array
* and also on the entryComponents array
*/
import { OrderPage } from '../pages/order/order';
import { ConfirmOrderPage } from '../pages/confirm-order/confirm-order';
import { PurchaseConfirmationPage } from '../pages/purchase-confirmation/purchase-confirmation';
import { ProductInfoPage } from '../pages/product-info/product-info';

///////////// User Pages /////////////
/*
* Each time you create a new page, add it on the declarations array
* and also on the entryComponents array
*/
//Take this as MyOrdersPage
import { OrdersPage } from '../pages/orders/orders';
//Take this as MyOrderDetailsPage
import { OrderDetailsPage } from '../pages/order-details/order-details';

///////////// Administration Pages /////////////
/*
* Each time you create a new page, add it on the declarations array
* and also on the entryComponents array
*/
import { AdministrationDashboardPage } from '../pages/administration-dashboard/administration-dashboard';
import { CreateProductPage } from '../pages/create-product/create-product';
import { UpdateProductPage } from '../pages/update-product/update-product';
import { MyProductsPage } from '../pages/my-products/my-products';
import { DayMenuPage } from '../pages/day-menu/day-menu';

///////////// Native Plugins /////////////
/*
* Each time you add a new native plugin, add it on the providers Array
*/
import { Camera } from '@ionic-native/camera';
import { DatePicker } from '@ionic-native/date-picker';
import { PayPal } from '@ionic-native/paypal';
import { Geolocation } from '@ionic-native/geolocation'

///////////// Native Plugins /////////////
/*
* Each time you add a new pipe, add it on the declarations Array
* Please on the //Pipes section
*/
import { LisToTextPipe } from '../app/pipes/list-to-text.pipe';

///////////// Maps /////////////
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    //Pages
    RegistrationPage,
    SignUpPage,
    FavouritesPage,
    OrderPage,
    ConfirmOrderPage,
    OrdersPage,
    PurchaseConfirmationPage,
    ProductInfoPage,
    OrderDetailsPage,
    SelectLocationPage,
    RestaurantRegistrationPage,
    AdministrationDashboardPage,
    SearchRestaurantsPage,
    CreateProductPage,
    MyProductsPage,
    DayMenuPage,
    UpdateProductPage,
    //Pipes,
    LisToTextPipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: 'Regresar',
      tabsHideOnSubPages: true,
    }),
    //Maps
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC8BlCTDf-VeD4R_8GlTTuwKBIQ9HaUhl4',
      libraries: ['places']
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    RegistrationPage,
    SignUpPage,
    FavouritesPage,
    OrderPage,
    ConfirmOrderPage,
    PurchaseConfirmationPage,
    OrdersPage,
    ProductInfoPage,
    OrderDetailsPage,
    SelectLocationPage,
    RestaurantRegistrationPage,
    AdministrationDashboardPage,
    SearchRestaurantsPage,
    CreateProductPage,
    MyProductsPage,
    DayMenuPage,
    UpdateProductPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpService,
    AuthenticationService,
    ValidEmailService,
    RestaurantsService,
    OrdersService,
    Camera,
    DatePicker,
    ImagesService,
    PayPal,
    ErrorsService,
    Geolocation,
    UserService,
    LocationService,
    ProductsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
