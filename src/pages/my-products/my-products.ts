import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { CreateProductPage } from '../create-product/create-product';
import { ProductsService } from '../../app/services/product.service';
import { UpdateProductPage } from '../update-product/update-product';

/**
 * Generated class for the MyProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-products',
  templateUrl: 'my-products.html',
})
export class MyProductsPage {

  public restaurant : any = {
    starters: [],
    midDish: [],
    mainDish: [],
    desserts: [],
    drinks: []
  };

  public deletedProduct : boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private modalController : ModalController,
    private alertController : AlertController,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private productsService : ProductsService) {
    this.restaurant = this.navParams.get('restaurant');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProductsPage');
  }

  goToCreateProductPage() {
    let modal = this.modalController.create( CreateProductPage );
    modal.onDidDismiss(data => {
      if( data.returnParam ) {
        this.appendProduct( data.product );
      }
    });
    modal.present();
  }

  appendProduct( product : any ) {
    let category = product.category;
    switch( category ){
      case 'Entradas': {
        this.restaurant.dayMenu.starters.push(product);
        break;
      }
      case 'Intermedios': {
        this.restaurant.dayMenu.midDish.push(product);
        break;
      }
      case 'Plato Fuerte': {
        this.restaurant.dayMenu.mainDish.push(product);
        break;
      }
      case 'Bebidas': {
        this.restaurant.dayMenu.drinks.push(product);
        break;
      }
      case 'Postres': {
        this.restaurant.dayMenu.desserts.push(product);
        break;
      }
    }
  }

  editProduct( item : any, index : number ) {

    console.log(item);

    item.addButtonStyle = !item.addButtonStyle;

    let modal = this.modalController.create( UpdateProductPage, { product: item } );
    modal.onDidDismiss(data => {
      if( data.returnParam ) {
        this.editProductFromView( data.product, index, item );
      }
    });
    modal.present();

    setTimeout(() => {
      item.addButtonStyle = !item.addButtonStyle;
    }, 150);

  }

  editProductFromView( product : any, index : number, previousData : any ) {
    let category = product.category;
    let previousCategory = previousData.category;
    switch( previousCategory ){
      case 'Entradas': {
        this.restaurant.dayMenu.starters[index].name = product.name;
        this.restaurant.dayMenu.starters[index].description = product.description;
        this.restaurant.dayMenu.starters[index].price = product.price;
        this.restaurant.dayMenu.starters[index].category = product.category;
        this.restaurant.dayMenu.starters[index].photo_url = product.photo_url;
        if ( category !== previousCategory ) {
          let p = this.restaurant.dayMenu.starters[index];
          this.restaurant.dayMenu.starters.splice(index, 1);
          this.appendProduct( p );
        }
        break;
      }
      case 'Intermedios': {
        this.restaurant.dayMenu.midDish[index].name = product.name;
        this.restaurant.dayMenu.midDish[index].description = product.description;
        this.restaurant.dayMenu.midDish[index].price = product.price;
        this.restaurant.dayMenu.midDish[index].category = product.category;
        this.restaurant.dayMenu.midDish[index].photo_url = product.photo_url;
        if ( category !== previousCategory ) {
          let p = this.restaurant.dayMenu.midDish[index];
          this.restaurant.dayMenu.midDish.splice(index, 1);
          this.appendProduct( p );
        }
        break;
      }
      case 'Plato Fuerte': {
        this.restaurant.dayMenu.mainDish[index].name = product.name;
        this.restaurant.dayMenu.mainDish[index].description = product.description;
        this.restaurant.dayMenu.mainDish[index].price = product.price;
        this.restaurant.dayMenu.mainDish[index].category = product.category;
        this.restaurant.dayMenu.mainDish[index].photo_url = product.photo_url;
        if ( category !== previousCategory ) {
          let p = this.restaurant.dayMenu.mainDish[index];
          this.restaurant.dayMenu.mainDish.splice(index, 1);
          this.appendProduct( p );
        }
        break;
      }
      case 'Bebidas': {
        this.restaurant.dayMenu.drinks[index].name = product.name;
        this.restaurant.dayMenu.drinks[index].description = product.description;
        this.restaurant.dayMenu.drinks[index].price = product.price;
        this.restaurant.dayMenu.drinks[index].category = product.category;
        this.restaurant.dayMenu.drinks[index].photo_url = product.photo_url;
        if ( category !== previousCategory ) {
          let p = this.restaurant.dayMenu.drinks[index];
          this.restaurant.dayMenu.drinks.splice(index, 1);
          this.appendProduct( p );
        }
        break;
      }
      case 'Postres': {
        this.restaurant.dayMenu.desserts[index].name = product.name;
        this.restaurant.dayMenu.desserts[index].description = product.description;
        this.restaurant.dayMenu.desserts[index].price = product.price;
        this.restaurant.dayMenu.desserts[index].category = product.category;
        this.restaurant.dayMenu.desserts[index].photo_url = product.photo_url;
        if ( category !== previousCategory ) {
          let p = this.restaurant.dayMenu.desserts[index];
          this.restaurant.dayMenu.desserts.splice(index, 1);
          this.appendProduct( p );
        }
        break;
      }
    }
  }



  deleteProduct( item : any, index : number ) {

    item.removeButtonStyle = !item.removeButtonStyle;
    let alert = this.alertController.create({
      title: '¿Estás seguro de eliminar este producto?',
      message: 'Selecciona Eliminar para eliminar el producto',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Eliminar',
          handler: () => {
            let data = {
              product: item,
            };

            let loading = this.loadingCtrl.create({
              content: 'Espera por favor...',
              spinner: 'bubbles'
            });

            loading.present();

            this.productsService.delete( JSON.stringify(data) ).subscribe(
              res => {
                this.deleteProductFromView( item, index );
                this.deletedProduct = true;
                loading.dismiss();
                setTimeout(() => {
                  this.deletedProduct = false;
                }, 1500);
              },
              error => {
                console.log("Error");
              }
            )
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });

    alert.present();
    setTimeout(() => {
      item.removeButtonStyle = !item.removeButtonStyle;
    }, 150);
  }

  deleteProductFromView( product : any, index : number ) {
    let category = product.category;
    switch( category ){
      case 'Entradas': {
        this.restaurant.dayMenu.starters.splice(index, 1);
        break;
      }
      case 'Intermedios': {
        this.restaurant.dayMenu.midDish.splice(index, 1);
        break;
      }
      case 'Plato Fuerte': {
        this.restaurant.dayMenu.mainDish.splice(index, 1);
        break;
      }
      case 'Bebidas': {
        this.restaurant.dayMenu.drinks.splice(index, 1);
        break;
      }
      case 'Postres': {
        this.restaurant.dayMenu.desserts.splice(index, 1);
        break;
      }
    }
  }

}
