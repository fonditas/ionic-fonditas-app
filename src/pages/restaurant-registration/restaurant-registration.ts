import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Restaurant } from '../../app/models/Restaurant';
import { User } from '../../app/models/User';
import { SelectLocationPage } from '../select-location/select-location';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LocationService } from '../../app/services/location.service';
import { AlertController } from 'ionic-angular';
import { ErrorsService } from '../../app/services/errors.service';
import { ImagesService } from '../../app/services/images.service';
import { AuthenticationService } from '../../app/services/authentication.service';
import { LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the RestaurantRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurant-registration',
  templateUrl: 'restaurant-registration.html',
})
export class RestaurantRegistrationPage {

  public restaurant : Restaurant = new Restaurant();
  private mediaSourceOption : any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private camera : Camera,
    public locationService : LocationService,
    private alertCtrl : AlertController,
    private modalCtrl: ModalController,
    private errorsService : ErrorsService,
    private imagesService : ImagesService,
    private authenticationService : AuthenticationService,
    private loadingCtrl : LoadingController) {
      this.mediaSourceOption = this.camera.PictureSourceType.PHOTOLIBRARY;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestaurantRegistrationPage');
  }

  ionViewWillEnter() {
    this.restaurant = new Restaurant();
  }

  showSelectLocationModal() {
    let locationModal = this.modalCtrl.create( SelectLocationPage );
    locationModal.present();
  }

  public changePhoto() {

    let alert = this.alertCtrl.create({
      title: 'Selecciona El Método Para Subir La Imagen',
      message: 'Tomar Una Foto / Seleccionar De La Galería',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Tomar Foto',
          handler: () => {
            this.mediaSourceOption = this.camera.PictureSourceType.CAMERA;
            this.showCamera();
          }
        },
        {
          text: 'Seleccionar De La Galería',
          handler: () => {
            this.mediaSourceOption = this.camera.PictureSourceType.PHOTOLIBRARY;
            this.showCamera();
          }
        }
      ]
    });
    alert.present();

  }

  private showCamera() {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType : this.mediaSourceOption,
      correctOrientation: true
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.restaurant.photo_dummy = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log("error in image loading");
    });
  }

  public imageText() {
    if ( this.restaurant.photo_dummy === 'assets/images/restaurant-thumbnail.svg' ) {
      return false;
    } else {
      return ( this.restaurant.name )? true : false;
    }
  }

  public register() {
    let errors = this.validateData();
    if ( errors.length > 0 ) {
      this.errorsService.showErrors(errors);
    } else {
      console.log("Storing data");
      this.storeData();
    }
  }

  public storeData() {

    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });

    loading.present();

    this.imagesService.upload( this.authenticationService.getPreservedUser().photo_dummy ).then( (url) => {

      this.authenticationService.getPreservedUser().photoBase64 = url;
      this.authenticationService.getPreservedUser().photo_dummy = '';

      this.imagesService.upload( this.restaurant.photo_dummy ).then( (url) => {

        this.restaurant.photo_url = url;
        this.restaurant.photo_dummy = '';

        let user : User = new User();
        let preservedUser = this.authenticationService.getPreservedUser();
        user.name = preservedUser.name;
        user.last_name = preservedUser.last_name;
        user.email = preservedUser.email;
        user.password = preservedUser.password;
        user.photoBase64 = preservedUser.photoBase64;

        this.restaurant.address = this.locationService.getLocalizationObject().address;
        this.restaurant.latitude = this.locationService.getLocalizationObject().latitude;
        this.restaurant.longitude = this.locationService.getLocalizationObject().longitude;


        let data = {
          user: user,
          restaurant: this.restaurant
        }


        this.authenticationService.registerWithRestaurant( JSON.stringify( data ) ).subscribe(
          res => {
            loading.dismiss();
            localStorage.setItem('token', `Bearer ${res.token}`);
            this.authenticationService.refreshUser();
            this.locationService.resetLocationObject();
            this.navCtrl.push( TabsPage, { selectedTab: 2 } );
          },
          error => {
            console.log(error);
            loading.dismiss();
          }
        );

      });



    });
  }

  public validateData() {
    let errors = [];
    this.trimData();
    if ( this.restaurant.name.length < 3 ) {
      errors.push( "El nombre del restaurante debe tener al menos 3 caracteres" );
    }
    if ( this.restaurant.description.length < 20 ) {
      errors.push( "La descripción del restaurante debe tener al menos 20 caracteres" );
    }
    if ( !this.locationService.getLocalizationObject().address ) {
      errors.push( "Debes seleccionar la ubicación de tu negocio" );
    }
    if ( this.restaurant.photo_dummy === 'assets/images/restaurant-thumbnail.svg' ) {
      errors.push( "Debes poner una foto de tu negocio" );
    }
    return errors;
  }

  public trimData() {
    this.restaurant.name = this.restaurant.name.trim();
    this.restaurant.description = this.restaurant.description.trim();
  }

}
