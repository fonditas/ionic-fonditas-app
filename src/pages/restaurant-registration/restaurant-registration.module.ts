import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantRegistrationPage } from './restaurant-registration';

@NgModule({
  declarations: [
    RestaurantRegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurantRegistrationPage),
  ],
})
export class RestaurantRegistrationPageModule {}
