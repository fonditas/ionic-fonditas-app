import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the OrderDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetailsPage {

  public order : any;
  public starters : any[] = [];
  public midDish : any[] = [];
  public mainDish : any[] = [];
  public desserts : any[] = [];
  public drinks : any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams) {

      this.order = this.navParams.get('order');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetailsPage');
  }

  ionViewWillEnter () {
    this.sortProductsByCategory();
  }

  sortProductsByCategory() {
    for ( let product of this.order.products) {
      switch( product.product.category ) {
        case 'Entradas': {
          this.starters.push( product );
          break;
        }
        case 'Intermedios': {
          this.midDish.push( product );
          break;
        }
        case 'Plato Fuerte': {
          this.mainDish.push( product );
          break;
        }
        case 'Bebidas': {
          this.drinks.push( product );
          break;
        }
        case 'Postres': {
          this.desserts.push( product );
          break;
        }
      }

    }
    console.log(this.starters);
    console.log(this.midDish);
    console.log(this.mainDish);
    console.log(this.drinks);
    console.log(this.desserts);
  }

}
