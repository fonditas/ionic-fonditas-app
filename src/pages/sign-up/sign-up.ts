import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../app/models/User';
import { ValidEmailService } from '../../app/services/valid-email.service';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AuthenticationService } from '../../app/services/authentication.service';
import { TabsPage } from  '../tabs/tabs';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagesService } from '../../app/services/images.service';
import { RestaurantRegistrationPage } from '../restaurant-registration/restaurant-registration';
import { ErrorsService } from '../../app/services/errors.service';

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  public user : User = new User();
  public passwordConfirmation : string = '';
  public asCostumer : boolean;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private validEmailService : ValidEmailService,
    private alertController : AlertController,
    private loadingCtrl : LoadingController,
    private authenticationService : AuthenticationService,
    private camera : Camera,
    private imagesService : ImagesService,
    private errorsService : ErrorsService) {

      this.asCostumer = this.navParams.get( 'asCostumer' );

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  ionViewWillEnter() {
    console.log("Entering");
    this.user = ( this.authenticationService.user )? this.authenticationService.user : new User();
    console.log(JSON.stringify(this.user));
  }


  public changePhoto() {

    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.user.photo_dummy = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log("error in image loading");
    });

  }

  public register() {

    let errors = this.userDataValidation();

    if ( errors.length == 0 ) {

      /*
      * Now we validate the password match
      */

      if ( this.user.password !== this.passwordConfirmation ){

        let alert = this.alertController.create({
          title: 'Error en el password',
          subTitle:'<ul class="align-left-errors"><li>El password y  su confirmación no coinciden</li></ul>',
          buttons: ['Cerrar']
        });
        alert.present();

      }else{

        /*
        * Here we must validate if the current email
        * is not taken in the backend
        */
        var data = {
          email : this.user.email
        }

        let loading = this.loadingCtrl.create({
          content: 'Espera por favor...',
          spinner: 'bubbles'
        });

        loading.present();

        console.log(JSON.stringify(data));
        this.validEmailService.checkAvailability(JSON.stringify(data)).subscribe(
          data => {
            if ( !data ){

              loading.dismiss();
              let alert = this.alertController.create({
                title: 'Error en el email',
                subTitle:'<ul class="align-left-errors"><li>El correo que ingresaste ya ha sido registrado</li></ul>',
                buttons: ['Cerrar']
              });
              alert.present();

            }else{

              this.imagesService.upload(this.user.photo_dummy).then( (url) => {
                this.user.photoBase64 = url;
                this.user.photo_dummy = '';
                var jsonUser = {
                  user : this.user
                }

                console.log(JSON.stringify(jsonUser));

                this.authenticationService.register(JSON.stringify(jsonUser)).subscribe(
                  data => {
                    this.authenticationService.refreshUser();
                    localStorage.setItem('token', `Bearer ${data}`);
                    loading.dismiss();
                    this.navCtrl.push(TabsPage);
                  },
                  error => {
                    loading.dismiss();
                    console.log(JSON.stringify(error));
                  }
                );

              });

            }
          },
          error => {
            console.log(error);
          }
        );

      }

    }else{
      this.errorsService.showErrors( errors );
    }
  }

  public next(){

    let errors = this.userDataValidation();
    if ( errors.length == 0 ) {

      if ( this.user.password !== this.passwordConfirmation ){

        let alert = this.alertController.create({
          title: 'Error en el password',
          subTitle:'<ul class="align-left-errors"><li>El password y  su confirmación no coinciden</li></ul>',
          buttons: ['Cerrar']
        });
        alert.present();

      } else {

        this.authenticationService.startPreservingUser( this.user );
        var data = {
          email : this.user.email
        }
        let loading = this.loadingCtrl.create({
          content: 'Espera por favor...',
          spinner: 'bubbles'
        });

        loading.present();

        this.validEmailService.checkAvailability( JSON.stringify( data ) ).subscribe(
          data => {
            if ( !data ) {
              loading.dismiss();
              let alert = this.alertController.create({
                title: 'Error en el email',
                subTitle:'<ul class="align-left-errors"><li>El correo que ingresaste ya ha sido registrado</li></ul>',
                buttons: ['Cerrar']
              });
              alert.present();
            } else {
              loading.dismiss();
              this.navCtrl.push( RestaurantRegistrationPage );
            }
          },
          error => {
            console.log("error");
          }
        );


      }

    } else {
      this.errorsService.showErrors( errors );
    }

  }

  public userDataValidation() {

    let errors = [];
    this.trimUserData();

    if ( this.user.name.length < 3 ){
      errors.push('El nombre debe tener al menos 3 caracteres');
    }

    if ( this.user.last_name.length < 3 ){
      errors.push('El apellido debe tener al menos 3 caracteres');
    }

    if ( !this.validEmailService.validateEmail(this.user.email) ){
      errors.push('Ingresa un email válido');
    }

    if ( this.user.password.length < 6 ){
      errors.push('El password debe tener al menos 6 caracteres');
    }

    if ( this.passwordConfirmation.length < 6 ){
      errors.push('La confirmación del password debe tener al menos 6 caracteres');
    }

    if ( this.user.photo_dummy === 'assets/images/profile-placeholder.png' ) {
      errors.push('Debes poner tu foto de perfil');
    }

    return errors;

  }

  private trimUserData() {

    this.user.name = this.user.name.trim();
    this.user.last_name = this.user.last_name.trim();
    this.user.email = this.user.email.trim();
    this.passwordConfirmation = this.passwordConfirmation.trim();

  }



}
