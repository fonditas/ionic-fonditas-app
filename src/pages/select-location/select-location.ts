import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocationService } from '../../app/services/location.service';
import { Location } from '../../app/models/Location';
import { AgmCoreModule } from '@agm/core';
import { MapsAPILoader } from '@agm/core';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

declare var google:any;


/**
 * Generated class for the SelectLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-location',
  templateUrl: 'select-location.html',
})
export class SelectLocationPage {

  private location : Location;
  public zoom : number;
  private loading;
  public firstLoad : boolean;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private locationService : LocationService,
    private mapsAPILoader : MapsAPILoader,
    private loadingCtrl : LoadingController,
    private alertController : AlertController) {
      this.location = new Location();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectLocationPage');
  }

  ionViewWillEnter() {

    this.firstLoad = this.locationService.firstLoad;

    this.loading = this.loadingCtrl.create({
      content: 'Estamos obteniendo tu ubicación...',
      spinner: 'bubbles'
    });
    this.loading.present();

    this.zoom = 17;
    this.loadCurrentPosition();
    this.autocomplete();

  }

  loadCurrentPosition() {

    let location = this.locationService.getLocalizationObject();
    if ( location.latitude ){
      let latlng = { lat: location.latitude , lng: location.longitude };
      this.getAddressFromCoordinates( latlng );
      return;
    }

    this.locationService.getLocalization().then(
      (resp) => {
        this.location = new Location();
        let latlng = { lat: resp.coords.latitude , lng: resp.coords.longitude };
        this.getAddressFromCoordinates( latlng );
      }
    ).catch(

      (error) => {
        console.log("Error getting location", error);
      }

    );

  }

  getAddressFromCoordinates( latlng : any ) {
    this.location.latitude = latlng.lat;
    this.location.longitude = latlng.lng;
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode( {'location': latlng}, (results, status) => {
      this.location.address = results[0].formatted_address;
      console.log(JSON.stringify(this.location));
      this.loading.dismiss();
      if ( this.firstLoad ) {
        this.firstLoad = false;
        this.presentStartAdvice();
      }
    });
  }

  autocomplete() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete( document.getElementById('autocomplete').getElementsByTagName('input')[0], {});
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.location.latitude  = place.geometry.location.lat();
        this.location.longitude = place.geometry.location.lng();
        this.location.address = place.formatted_address;
        console.log(place);
      });
    });
  }

  addMarker( $event : any) {
    let latlng = { lat: $event.coords.lat , lng: $event.coords.lng };
    this.getAddressFromCoordinates( latlng );
  }

  presentStartAdvice() {
    let alert = this.alertController.create({
      title: 'Selecciona la ubicación de tu negocio',
      subTitle: `<p>Autómaticamente pusimos tu ubicación actual como la dirección de tu negocio. Da click en el mapa para seleccionar y cambiar la ubicación o usa el buscador de la parte superior para encontrar la ubicación de tu negocio.`,
      buttons: ['Entendido'],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  confirmLocation() {
    console.log("Confirming location");
    this.locationService.setLocalizationObject( this.location );
    this.locationService.firstLoad = false;
    this.navCtrl.pop();
  }

}
