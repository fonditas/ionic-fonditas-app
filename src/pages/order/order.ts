import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Cart } from '../../app/models/Cart';
import { OrdersService } from '../../app/services/orders.service';
import { ConfirmOrderPage } from '../confirm-order/confirm-order';
import { ProductInfoPage } from '../product-info/product-info';

/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  showCartButton : boolean = false;
  restaurant : any;
  cart : Cart;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private orderService : OrdersService,
    public modalCtrl: ModalController ) {
    this.restaurant = this.navParams.get('restaurant');
    this.cart = new Cart();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
    console.log(this.restaurant);
  }


  addItem(item : any) {
    item.addButtonStyle = !item.addButtonStyle;
    this.cart.addItem( item );
    setTimeout(() => {
      item.addButtonStyle = !item.addButtonStyle;
    }, 150);
  }

  removeItem( item : any) {
    item.removeButtonStyle = !item.removeButtonStyle;
    this.cart.removeItem( item );
    setTimeout(() => {
      item.removeButtonStyle = !item.removeButtonStyle;
    }, 150);
  }

  order() {
    /*this.cart.restaurantId = this.restaurant.id;
    this.cart.hourOfArrive = new Date();
    this.cart.mergeAllProducts();
    let order = {
      order : this.cart
    }
    console.log(JSON.stringify(order));
    this.orderService.order(JSON.stringify(order)).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );*/
    this.navCtrl.push( ConfirmOrderPage, { restaurant: this.restaurant, order: this.cart }  );
  }

  showItemInfo( item : any ) {
    let profileModal = this.modalCtrl.create(
      ProductInfoPage, { product : item } );
    profileModal.present();
  }




}
