import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { FavouritesPage } from '../favourites/favourites';
import { OrdersPage } from '../orders/orders';
import { SearchRestaurantsPage } from '../search-restaurants/search-restaurants';
import { AdministrationDashboardPage } from '../administration-dashboard/administration-dashboard';
import { AuthenticationService } from '../../app/services/authentication.service';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {


  tab1Root = FavouritesPage;
  tab2Root = SearchRestaurantsPage;
  tab3Root = AdministrationDashboardPage;
  tab4Root = OrdersPage;
  tab5Root = ContactPage;
  selectedIndex = 0;


  isOwner : boolean;

  constructor(public authenticationService : AuthenticationService,
    private navParams : NavParams) {
    this.isOwner = this.authenticationService.isOwner();
    this.selectedIndex = this.navParams.get('selectedTab') || 0;
  }

}
