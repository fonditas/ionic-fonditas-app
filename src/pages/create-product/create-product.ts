import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../app/services/product.service';
import { Product } from '../../app/models/Product';
import { ErrorsService } from '../../app/services/errors.service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagesService } from '../../app/services/images.service';

/**
 * Generated class for the CreateProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-product',
  templateUrl: 'create-product.html',
})
export class CreateProductPage {

  public product : any;
  private loading : any;
  private mediaSourceOption : any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private productsService : ProductsService,
    private errorsService : ErrorsService,
    public viewCtrl: ViewController,
    private loadingCtrl : LoadingController,
    private alertCtrl : AlertController,
    private camera : Camera,
    private imagesService : ImagesService) {

      this.product = this.navParams.get('product') || new Product();
      this.loading = this.loadingCtrl.create({
        content: 'Espera por favor...',
        spinner: 'bubbles'
      });
      this.mediaSourceOption = this.camera.PictureSourceType.PHOTOLIBRARY;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProductPage');
  }

  createProduct() {
    let errors = this.validateData();

    if ( errors.length == 0 ) {

      this.loading.present();
      if ( this.product.photo_dummy === "assets/images/food.jpg" ) {

        let data = {
          product: this.product
        }
        this.productsService.store( JSON.stringify( data ) ).subscribe(
          product => {
            this.product = product;
            this.loading.dismiss();
            this.dismiss( true );
          },
          error => {
            console.log("error");
          }
        );


      } else {
        console.log("With image");
        this.imagesService.upload(this.product.photo_dummy).then( (url) => {
          this.product.photo_url = url;
          this.product.photo_dummy = '';
          let data = {
            product: this.product
          }

          this.productsService.store( JSON.stringify( data ) ).subscribe(
            product => {
              this.product = product;
              this.loading.dismiss();
              this.dismiss( true );
            },
            error => {
              console.log("error");
            }
          );

        });

      }

    } else {
      this.errorsService.showErrors(errors);
    }
  }

  validateData() {
    let errors = [];
    this.trimAndStyleData();
    if ( this.product.name.length < 3 ){
      errors.push( "El nombre del producto debe tener al menos 3 caracteres" );
    }
    if ( this.product.description.length < 20 ){
      errors.push( "La descripción debe tener al menos 20 caracteres" );
    }
    if ( this.product.category.length == 0 ) {
      errors.push( "Debes escoger una categoría" );
    }
    if ( !this.product.price ) {
      errors.push( "Debes ingresar el precio del producto" );
    }
    return errors;
  }

  trimAndStyleData() {
    this.product.name = this.capitalize( this.capitalize ( this.product.name.trim() ) );
    this.product.description = this.capitalize( this.capitalize ( this.product.description.trim() ) );
  }

  capitalize( string : string ) {
    var splitStr = string.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

  closeModal(){
    this.dismiss(false);
  }

  dismiss( returnParam : boolean ) {
    let data = { 'product': this.product, 'returnParam': returnParam };
    this.viewCtrl.dismiss( data );
  }

  public changePhoto() {

    let alert = this.alertCtrl.create({
      title: 'Selecciona El Método Para Subir La Imagen',
      message: 'Tomar Una Foto / Seleccionar De La Galería',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Tomar Foto',
          handler: () => {
            this.mediaSourceOption = this.camera.PictureSourceType.CAMERA;
            this.showCamera();
          }
        },
        {
          text: 'Seleccionar De La Galería',
          handler: () => {
            this.mediaSourceOption = this.camera.PictureSourceType.PHOTOLIBRARY;
            this.showCamera();
          }
        }
      ]
    });
    alert.present();

  }

  private showCamera() {
    const cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType : this.mediaSourceOption,
      correctOrientation: true
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.product.photo_dummy = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log("error in image loading");
    });
  }

  public imageText() {
    if ( this.product.photo_dummy === 'assets/images/food.jpg' ) {
      return false;
    } else {
      return ( this.product.name )? true : false;
    }
  }


}
