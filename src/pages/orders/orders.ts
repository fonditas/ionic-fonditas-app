import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../app/services/user.service';
import * as moment from 'moment';
import { LoadingController } from 'ionic-angular';
import { OrderDetailsPage } from  '../order-details/order-details';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  public orders : any[] = [];
  public pastOrders : any[] = [];
  public comingOrders : any[] = [];
  public showingPastOrders : boolean;
  public finishLoading : boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private userService : UserService,
    private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }

  ionViewWillEnter() {
    this.finishLoading = false;
    this.updatePageData();
    this.showingPastOrders = true;
  }

  updatePageData(){
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();
    this.userService.orders().subscribe(
      data => {
        this.orders = [];
        this.comingOrders = [];
        this.pastOrders = [];
        data.reverse();
        for ( let o of data ) {
          o.created_at = this.getDateInFormat( o.created_at );
          if ( this.isAComingOrder( o.hour_of_arrive ) ) {
            o.hour_of_arrive = this.getHourInFormat( o.hour_of_arrive );
            this.comingOrders.push(o);
          } else {
            o.hour_of_arrive = this.getHourInFormat( o.hour_of_arrive );
            this.pastOrders.push(o);
          }
        }
        this.orders = this.pastOrders;
        this.finishLoading = true;
        loading.dismiss();
      },
      error => {
        console.log(error);
      }
    );
  }

  public isAComingOrder(date : any){
    return moment(date).utc().utcOffset('-1200').isAfter(moment().utc().utcOffset('-1200'));
  }

  public getHourInFormat( date : string ) {
    let momentDate = moment(date).locale('es-MX').calendar();
    return momentDate;
  }

  public getDateInFormat(date : string) {
    let momentDate = moment(date).utc().utcOffset('-1200').locale('es-MX').format('LLLL');
    return momentDate;
  }

  public switchShowedOrders() {
    this.orders = ( this.showingPastOrders )? this.comingOrders : this.pastOrders;
    this.showingPastOrders = !this.showingPastOrders;
  }

  public showDetails( order : any ) {
    this.navCtrl.push( OrderDetailsPage, { order: order} );
  }

}
