import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../app/services/product.service';

/**
 * Generated class for the DayMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-day-menu',
  templateUrl: 'day-menu.html',
})
export class DayMenuPage {

  public currentMenu : any = {
    starters: [],
    midDish: [],
    mainDish: [],
    desserts: [],
    drinks: []
  }

  public publishedMenu : any = {
    starters: [],
    midDish: [],
    mainDish: [],
    desserts: [],
    drinks: []
  }

  public showData : any = {
    starters: [],
    midDish: [],
    mainDish: [],
    desserts: [],
    drinks: []
  };

  public dayMenu : any;
  public open : boolean = true;

  public showingProducts : boolean = true;
  public showAddedAdvice : boolean = false;
  public showRemovedAdvice : boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private productsService : ProductsService,
    private alertCtrl : AlertController,
    private loadingCtrl : LoadingController) {
    this.dayMenu = this.navParams.get('dayMenu');
    this.open = this.navParams.get('open');
  }

  public refreshAll() {
    this.showData = {
      starters: [],
      midDish: [],
      mainDish: [],
      desserts: [],
      drinks: []
    };
    this.publishedMenu = {
      starters: [],
      midDish: [],
      mainDish: [],
      desserts: [],
      drinks: []
    };
    this.currentMenu = {
      starters: [],
      midDish: [],
      mainDish: [],
      desserts: [],
      drinks: []
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DayMenuPage');
  }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();
    this.refreshAll();
    this.sortData();
    this.showingProducts = true;
    this.showData = this.currentMenu;
    loading.dismiss();
  }

  public confirmChanges() {
    console.log("confirm changfes");
    this.showAlertBeforeSendData();
  }


  showAlertBeforeSendData() {
    let alert = this.alertCtrl.create({
      title: '¿Estás seguro de los cambios?',
      message: 'Si estás seguto oprime Confirmar',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Confirmar',
          handler: () => {
            this.publishChanges();
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            return;
          }
        }
      ]
    });
    alert.present();
  }

  public publishChanges() {

    let data = this.getPublishedIds();

    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();

    this.productsService.productsActive( JSON.stringify(data) ).subscribe(
      res => {
        console.log(res);
        loading.dismiss();
        this.navCtrl.pop();
      },
      error => {
        console.log(error);
      }
    );
  }

  public getPublishedIds() {
    let ids = [];
    let sold = [];
    for ( let item of this.publishedMenu.starters ) {
      ids.push( item.id );
      if ( item.sold_out ){
        sold.push( item.id );
      }
    }

    for ( let item of this.publishedMenu.midDish ) {
      ids.push( item.id );
      if ( item.sold_out ){
        sold.push( item.id );
      }
    }

    for ( let item of this.publishedMenu.mainDish ) {
      ids.push( item.id );
      if ( item.sold_out ){
        sold.push( item.id );
      }
    }

    for ( let item of this.publishedMenu.desserts ) {
      ids.push( item.id );
      if ( item.sold_out ){
        sold.push( item.id );
      }
    }

    for ( let item of this.publishedMenu.drinks ) {
      ids.push( item.id );
      if ( item.sold_out ){
        sold.push( item.id );
      }
    }
    let data = {
      products: ids,
      soldOuts: sold
    }
    return data;
  }

  public switchData() {
    this.showingProducts = !this.showingProducts;
    this.showData = (this.showingProducts)? this.currentMenu : this.publishedMenu;
  }

  public addProductToDayMenu( item : any, index : number ) {
    if ( this.showRemovedAdvice ) {
      this.showRemovedAdvice = false;
    }
    this.showAddedAdvice = true;
    this.pushProductToCorrectCategory ( item, index );
    setTimeout(() => {
      this.showAddedAdvice = false;
    }, 1500);
    console.log(item);
  }

  public removeProductFromDayMenu( item : any, index : number ) {
    if ( this.showAddedAdvice ) {
      this.showAddedAdvice = false;
    }
    this.showRemovedAdvice = true;
    this.pushProductToCorrectCategory ( item, index );
    setTimeout(() => {
      this.showRemovedAdvice = false;
    }, 1500);
    console.log(item);
  }

  public markAsSoldOut( item : any, index : number ) {
    item.sold_out = true;
  }


  private pushProductToCorrectCategory( item : any, index : number ) {
    let category = item.category;
    switch( category ){
      case 'Entradas': {
        if ( !item.active ) {
          this.currentMenu.starters[index].active = true;
          this.currentMenu.starters.splice(index, 1);
          this.publishedMenu.starters.push(item);
        } else {
          this.publishedMenu.starters[index].active = false;
          this.publishedMenu.starters.splice(index, 1);
          this.currentMenu.starters.push(item);
        }
        break;
      }
      case 'Intermedios': {
        if ( !item.active ) {
          this.currentMenu.midDish[index].active = true;
          this.currentMenu.midDish.splice(index, 1);
          this.publishedMenu.midDish.push(item);
        } else {
          this.publishedMenu.midDish[index].active = false;
          this.publishedMenu.midDish.splice(index, 1);
          this.currentMenu.midDish.push(item);
        }
        break;
      }
      case 'Plato Fuerte': {
        if ( !item.active ) {
          this.currentMenu.mainDish[index].active = true;
          this.currentMenu.mainDish.splice(index, 1);
          this.publishedMenu.mainDish.push(item);
        } else {
          this.publishedMenu.mainDish[index].active = false;
          this.publishedMenu.mainDish.splice(index, 1);
          this.currentMenu.mainDish.push(item);
        }
        break;
      }
      case 'Bebidas': {
        if ( !item.active ) {
          this.currentMenu.drinks[index].active = true;
          this.currentMenu.drinks.splice(index, 1);
          this.publishedMenu.drinks.push(item);
        } else {
          this.publishedMenu.drinks[index].active = false;
          this.publishedMenu.drinks.splice(index, 1);
          this.currentMenu.drinks.push(item);
        }
        break;
      }
      case 'Postres': {
        if ( !item.active ) {
          this.currentMenu.desserts[index].active = true;
          this.currentMenu.desserts.splice(index, 1);
          this.publishedMenu.desserts.push(item);
        } else {
          this.publishedMenu.desserts[index].active = false;
          this.publishedMenu.desserts.splice(index, 1);
          this.currentMenu.desserts.push(item);
        }
        break;
      }
    }
  }

  public sortData() {
    for( let item of this.dayMenu.starters ){
      if ( item.active ) {
        this.publishedMenu.starters.push( item );
      } else {
        this.currentMenu.starters.push( item );
      }
    }
    for( let item of this.dayMenu.midDish ){
      if ( item.active ) {
        this.publishedMenu.midDish.push( item );
      } else {
        this.currentMenu.midDish.push( item );
      }
    }
    for( let item of this.dayMenu.mainDish ){
      if ( item.active ) {
        this.publishedMenu.mainDish.push( item );
      } else {
        this.currentMenu.mainDish.push( item );
      }
    }
    for( let item of this.dayMenu.desserts ){
      if ( item.active ) {
        this.publishedMenu.desserts.push( item );
      } else {
        this.currentMenu.desserts.push( item );
      }
    }
    for( let item of this.dayMenu.drinks ){
      if ( item.active ) {
        this.publishedMenu.drinks.push( item );
      } else {
        this.currentMenu.drinks.push( item );
      }
    }
  }


}
