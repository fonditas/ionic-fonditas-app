import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayMenuPage } from './day-menu';

@NgModule({
  declarations: [
    DayMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(DayMenuPage),
  ],
})
export class DayMenuPageModule {}
