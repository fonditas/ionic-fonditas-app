import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdministrationDashboardPage } from './administration-dashboard';

@NgModule({
  declarations: [
    AdministrationDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(AdministrationDashboardPage),
  ],
})
export class AdministrationDashboardPageModule {}
