import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserService } from '../../app/services/user.service';
import { CreateProductPage } from '../create-product/create-product';
import { MyProductsPage } from '../my-products/my-products';
import { DayMenuPage } from '../day-menu/day-menu';

/**
 * Generated class for the AdministrationDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-administration-dashboard',
  templateUrl: 'administration-dashboard.html',
})
export class AdministrationDashboardPage {

  public restaurant : any = {
    open: false,
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private userService : UserService,
    private loadingCtrl : LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdministrationDashboardPage');
  }

  ionViewWillEnter(){
    this.updatePageData();
  }

  updatePageData(){
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();
    this.userService.myRestaurant().subscribe(
      restaurant => {
        this.restaurant = restaurant;
        console.log(this.restaurant);
        this.orderDishes();
        loading.dismiss();
      },
      error => {
        console.log(error);
        loading.dismiss();
      }
    );
  }

  public myRestaurantOpenClose(){
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();
    this.userService.myRestaurantOpenClose().subscribe(
      restaurant => {
        this.restaurant = restaurant;
        this.orderDishes();
        loading.dismiss();
      },
      error => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  public goToMyProductsPage() {
    this.navCtrl.push( MyProductsPage, { 'restaurant': this.restaurant } );
  }

  public goToDayMenuPage() {
    let data = {
      'dayMenu': this.restaurant.dayMenu,
      'open': this.restaurant.open
    }
    this.navCtrl.push( DayMenuPage, data);
  }

  private orderDishes() {
    for ( let product of this.restaurant.products ) {

      product['removeButtonStyle'] = false;
      product['addButtonStyle'] = false;

      switch( product.category ) {
        case 'Entradas': {
          this.restaurant.dayMenu.starters.push(product);
          break;
        }
        case 'Intermedios': {
          this.restaurant.dayMenu.midDish.push(product);
          break;
        }
        case 'Plato Fuerte': {
          this.restaurant.dayMenu.mainDish.push(product);
          break;
        }
        case 'Bebidas': {
          this.restaurant.dayMenu.drinks.push(product);
          break;
        }
        case 'Postres': {
          this.restaurant.dayMenu.desserts.push(product);
          break;
        }
      }

    }
  }

}
