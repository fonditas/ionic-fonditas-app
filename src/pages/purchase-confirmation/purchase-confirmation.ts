import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the PurchaseConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase-confirmation',
  templateUrl: 'purchase-confirmation.html',
})
export class PurchaseConfirmationPage {

  public order : any;
  public restaurant : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.restaurant = this.navParams.get('restaurant');
    this.order = this.navParams.get('order');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PurchaseConfirmationPage');
  }

  goToTabs() {
    this.navCtrl.push(TabsPage);
  }

}
