import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ValidEmailService } from '../../app/services/valid-email.service';
import { TabsPage } from  '../tabs/tabs';
import { SignUpPage } from '../sign-up/sign-up';
import { AlertController } from 'ionic-angular';
import { AuthenticationService } from '../../app/services/authentication.service';
import { LoadingController } from 'ionic-angular';
import { SelectLocationPage } from '../select-location/select-location';
import { AdministrationDashboardPage } from '../administration-dashboard/administration-dashboard';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  public email : string = '';
  public password : string = '';
  public showContent : boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private validEmailService : ValidEmailService,
    private alertController : AlertController,
    private authenticationService : AuthenticationService,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {

    if ( localStorage.getItem('token') ){

      setTimeout(() => {
        if ( this.authenticationService.isOwner() ) {
          this.navCtrl.push( TabsPage, { selectedTab: 2 } );
        } else {
          this.navCtrl.push( TabsPage );
        }
      }, 1300);

    }else{
      this.showContent = true;
    }

  }

  ionViewWillEnter() {
  }



  login(){

    var errors = [];
    var emailError = false;
    var passwordError = false;

    this.email = this.email.trim();

    if ( !this.validEmailService.validateEmail(this.email) ){

      errors.push('La dirección de correo no es valida');
      emailError = true;

    }

    if ( this.password.length < 6 ){
      errors.push('La contraseña debe tener al menos 6 caracteres');
      passwordError = true;
    }

    if ( errors.length == 2){

      let alert = this.alertController.create({
        title: 'Error en tus datos',
        subTitle: `<ul class="align-left-errors"><li>${errors[0]}</li><li>${errors[1]}</li><ul>`,
        buttons: ['Cerrar']
      });
      alert.present();

    }else if (errors.length == 1){

      var title = '';

      if ( emailError ){
        title = 'Error en el correo';
      }else if ( passwordError ) {
        title = 'Error en el password';
      }

      let alert = this.alertController.create({
        title: title,
        subTitle: `<ul class="align-left-errors"><li>${errors[0]}</li></ul>`,
        buttons: ['Cerrar']
      });
      alert.present();

    }else{

      var data : any = {
        email: this.email,
        password: this.password
      };

      let loading = this.loadingCtrl.create({
        content: 'Espera por favor...',
        spinner: 'bubbles'
      });

      loading.present();

      this.authenticationService.authenticate(JSON.stringify(data)).subscribe(
        token => {
          localStorage.setItem('token', `Bearer ${token}`);
          loading.dismiss();
          if ( this.authenticationService.isOwner() ) {
            this.navCtrl.push( TabsPage, { selectedTab: 2 } );
          } else {
            this.navCtrl.push( TabsPage );
          }
        },
        error => {

          loading.dismiss();

          let alert = this.alertController.create({
            title: 'Credenciales incorrectas',
            subTitle: `Los datos que ingresaste no son correctos`,
            buttons: ['Cerrar']
          });
          alert.present();

        }
      );
    }

  }

  goToSignUp() {
    this.navCtrl.push( SignUpPage, { asCostumer: true } );
  }

  goToSignUpWithRestaurant() {
    this.navCtrl.push( SignUpPage, { asCostumer: false } );
  }

  goToMapModal() {
    let modal = this.modalCtrl.create( SelectLocationPage );
    modal.present();
  }


}
