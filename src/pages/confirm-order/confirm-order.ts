import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { AlertController } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { OrdersService } from '../../app/services/orders.service';
import { PurchaseConfirmationPage } from '../purchase-confirmation/purchase-confirmation';

/**
 * Generated class for the ConfirmOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-order',
  templateUrl: 'confirm-order.html',
})
export class ConfirmOrderPage {

  restaurant : any;
  order : any;
  hourOfArrive : string;
  public enableDatePicker:boolean = true;
  public dateOptions : any = {
    year: "numeric",
    month: "short",
    day: "numeric"
  };

  private payPalEnvironment: string = 'payPalEnvironmentSandbox';
  private enviromentProduction = '';
  private enviromentSandBox = 'AYSPqrplGzwNSmob04OA1u5Y_oZ8-F4Ynv5lr3uNmlQgJYuRVGkuvmkmupgqJdQaknQvxYJ4yULG-sXp';

  constructor(public navCtrl : NavController,
    public navParams : NavParams,
    private datePicker : DatePicker,
    private platform : Platform,
    private alertController : AlertController,
    private payPal : PayPal,
    private ordersService : OrdersService) {

    this.restaurant = this.navParams.get('restaurant');
    this.order = this.navParams.get('order');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmOrderPage');
  }

  ionViewWillEnter() {
    this.enableDatePicker = this.platform.is('ios') || this.platform.is('android')
  }

  showDatePicker(){
    let now = new Date();
    this.datePicker.show({
      date: (this.order.hourOfArrive)? this.order.hourOfArrive : new Date(),
      mode: 'time',
      is24Hour: true,
      allowOldDates: false
    }).then(
      date => {
        this.order.hourOfArrive = date;
        this.dateChange();
      }
    ).catch((e) => {
      console.log(e);
    });
  }

  dateChange(){
    this.hourOfArrive = this.order.hourOfArrive;
  }

  procedToPayment(){
    if ( !this.order.hourOfArrive ) {
      let alert = this.alertController.create({
        title: 'Confirma la hora de tu llegada',
        subTitle: `Por favor confirma a que hora llegarás a comer`,
        buttons: ['Cerrar']
      });
      alert.present();
    } else {
      this.payPalPayment();
    }
  }

  payPalPayment() {
    this.payPal.init({
      PayPalEnvironmentProduction: this.enviromentProduction,
      PayPalEnvironmentSandbox: this.enviromentSandBox
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(`${this.order.total.toFixed(2)}`, 'USD', 'Description', 'rica comida');
        this.payPal.renderSinglePaymentUI(payment).then((response) => {

          this.order.paypalId = response.response.id;
          this.order.restaurantId = this.restaurant.id;
          this.order.mergeAllProducts();

          let JSONorder = {
            order : this.order
          }
          this.ordersService.order(JSON.stringify(JSONorder)).subscribe(
            data => {
              console.log(data);
              this.navCtrl.push( PurchaseConfirmationPage, { restaurant: this.restaurant, order: this.order }  );
            },
            error => {
              console.log(error);
            }
          );
          // Successfully paid

          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, (errorDialog) => {
          let alert = this.alertController.create({
            title: 'No has procedido al pago',
            subTitle: `Has cerrado la ventana de pago y no pudimos completar tu compra`,
            buttons: ['Cerrar']
          });
          alert.present();
        });
      }, (errorConfiguration) => {
        let alert = this.alertController.create({
          title: 'Error',
          subTitle: `Error en la configuración de paypal`,
          buttons: ['Cerrar']
        });
        alert.present();
      });
    }, (errorInitialization) => {
      let alert = this.alertController.create({
        title: 'Error - I',
        subTitle: `Error en la inicializacion`,
        buttons: ['Cerrar']
      });
      alert.present();
    });
  }

}
