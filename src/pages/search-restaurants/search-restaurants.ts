import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController, AlertController } from 'ionic-angular';
import { RestaurantsService } from '../../app/services/restaurants.service';
import { OrderPage } from '../order/order';

/**
 * Generated class for the SearchRestaurantsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-restaurants',
  templateUrl: 'search-restaurants.html',
})
export class SearchRestaurantsPage {

  restaurants : any[] = [

  ];

  favourites : number[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private restaurantsService : RestaurantsService,
    private loadingCtrl : LoadingController,
    private alertController : AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchRestaurantsPage +');
  }

  ionViewWillEnter() {
    this.favourites = [];
    this.updatePageData();
  }

  updatePageData() {
    console.log("Updateing");
    let loading = this.loadingCtrl.create({
      content: 'Estamos obteniendo la información de las fondas 😋',
      spinner: 'bubbles'
    });
    loading.present();

    this.restaurantsService.favourites().subscribe(
      restaurants => {
        for ( let r of restaurants) {
          this.favourites.push(r.id);
        }
        console.log(this.favourites);
        this.restaurantsService.restaurants().subscribe(
          data => {
            let restaurantsData = data;

            for ( let restaurant of restaurantsData) {

              restaurant['favourite'] = false;
              restaurant['favourite'] = this.isFavourite(restaurant);

              for ( let product of restaurant.products ) {

                product['removeButtonStyle'] = false;
                product['addButtonStyle'] = false;

                switch( product.category ) {
                  case 'Entradas': {
                    restaurant.dayMenu.starters.push(product);
                    break;
                  }
                  case 'Intermedios': {
                    restaurant.dayMenu.midDish.push(product);
                    break;
                  }
                  case 'Plato Fuerte': {
                    restaurant.dayMenu.mainDish.push(product);
                    break;
                  }
                  case 'Bebidas': {
                    restaurant.dayMenu.drinks.push(product);
                    break;
                  }
                  case 'Postres': {
                    restaurant.dayMenu.desserts.push(product);
                    break;
                  }
                }

              }

            }

            this.restaurants = restaurantsData;
            console.log(this.restaurants);
            loading.dismiss();

          },
          error => {
            console.log(error);
            loading.dismiss();
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  isFavourite( item : any ) {
    for( let r of this.favourites ){
      if ( r == item.id ){
        return true;
      }
    }
    return false;
  }


  deleteFavourite( restaurant : any, index : number ) {
    let alert = this.alertController.create({
      title: '¿Estás seguro de quitar esta fonda de tus favoritos?',
      message: 'Selecciona Quitar para confirmar',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Quitar',
          handler: () => {
            this.confirmDelete( restaurant, index );
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  confirmDelete( restaurant : any, index : number ) {
    let data = {
      restaurant: restaurant
    }
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });

    loading.present();
    let ind = 0;
    for(let i of this.favourites){
      if( i == restaurant.id ){
        this.favourites.splice(ind, 1);
        break;
      }
      i++;
    }
    this.restaurantsService.deleteFavourite( JSON.stringify( data ) ).subscribe(
      res => {
        this.restaurants[index].favourite = false;
        loading.dismiss();
      },
      error => {
        console.log(error);
      }
    );
  }

  addFavourite( restaurant : any, index : number ) {
    let alert = this.alertController.create({
      title: '¿Estás seguro de añadir esta fonda de tus favoritos?',
      message: 'Selecciona Añadir para confirmar',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Añadir',
          handler: () => {
            this.confirmAdd( restaurant, index );
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  confirmAdd( restaurant : any, index : number ) {
    let data = {
      restaurant: restaurant
    }
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });
    loading.present();
    this.restaurantsService.addFavourite( JSON.stringify( data ) ).subscribe(
      res => {
        this.favourites = res;
        this.restaurants[index].favourite = true;
        loading.dismiss();
      },
      error => {
        console.log(error);
      }
    );
  }

  goToOrder(restaurant : any) {
    this.navCtrl.push( OrderPage, { restaurant: restaurant } );
  }

}
