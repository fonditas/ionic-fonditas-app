import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchRestaurantsPage } from './search-restaurants';

@NgModule({
  declarations: [
    SearchRestaurantsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchRestaurantsPage),
  ],
})
export class SearchRestaurantsPageModule {}
