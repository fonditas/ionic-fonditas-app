import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Product } from '../../app/models/Product';
import { ProductsService } from '../../app/services/product.service';

/**
 * Generated class for the UpdateProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-product',
  templateUrl: 'update-product.html',
})
export class UpdateProductPage {

  product : Product = new Product();

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private productsService : ProductsService,
    public viewCtrl : ViewController,
    private loadingCtrl: LoadingController) {
    let product = this.navParams.get('product');
    this.product.id = product.id;
    this.product.name = product.name;
    this.product.description = product.description;
    this.product.category = product.category;
    this.product.photo_url = product.photo_url;
    this.product.price = product.price;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateProductPage');
  }

  updateProduct() {

    let data = {
      product: this.product
    };
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });

    loading.present();

    this.productsService.update( JSON.stringify(data) ).subscribe(
      res => {
        loading.dismiss();
        this.product = res;
        this.dismissWithData( res );
      },
      error => {
        console.log(error)
        loading.dismiss();
      }
    );

  }


  closeModal(){
    this.dismiss(false);
  }

  dismiss( returnParam : boolean ) {
    let data = { 'product': this.product, 'returnParam': returnParam };
    this.viewCtrl.dismiss( data );
  }

  dismissWithData( prod : any ) {
    let data = { 'product': prod, 'returnParam': true };
    this.viewCtrl.dismiss( data );
  }

}
