import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OrderPage } from '../order/order';
import { RestaurantsService } from '../../app/services/restaurants.service';
import { LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the FavouritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favourites',
  templateUrl: 'favourites.html',
})
export class FavouritesPage {

  restaurants : any[] = [

  ];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private restaurantsService : RestaurantsService,
    private loadingCtrl : LoadingController,
    private alertController : AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavouritesPage');
  }

  ionViewWillEnter() {
    console.log('ionViewEnter FavouritesPage');
    this.updatePageData();
  }

  updatePageData() {

    let loading = this.loadingCtrl.create({
      content: 'Estamos obteniendo la información de tus fondas favoritas 😋',
      spinner: 'bubbles'
    });

    loading.present();
    this.restaurantsService.favourites().subscribe(
      data => {

        let restaurantsData = data;

        for ( let restaurant of restaurantsData) {

          for ( let product of restaurant.products ) {

            product['removeButtonStyle'] = false;
            product['addButtonStyle'] = false;

            if ( product.active ) {
              switch( product.category ) {
                case 'Entradas': {
                  restaurant.dayMenu.starters.push(product);
                  break;
                }
                case 'Intermedios': {
                  restaurant.dayMenu.midDish.push(product);
                  break;
                }
                case 'Plato Fuerte': {
                  restaurant.dayMenu.mainDish.push(product);
                  break;
                }
                case 'Bebidas': {
                  restaurant.dayMenu.drinks.push(product);
                  break;
                }
                case 'Postres': {
                  restaurant.dayMenu.desserts.push(product);
                  break;
                }
              }
            }

          }

        }

        this.restaurants = restaurantsData;
        loading.dismiss();

      },
      error => {
        console.log(error);
        loading.dismiss();
      }
    );
  }

  goToOrder(restaurant : any) {
    this.navCtrl.push( OrderPage, { restaurant: restaurant } );
  }

  deleteFavourite( restaurant : any, index : number ) {
    let alert = this.alertController.create({
      title: '¿Estás seguro de quitar esta fonda de tus favoritos?',
      message: 'Selecciona Quitar para confirmar',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Quitar',
          handler: () => {
            this.confirmDelete( restaurant, index );
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  confirmDelete( restaurant : any, index : number ) {
    let data = {
      restaurant: restaurant
    }
    let loading = this.loadingCtrl.create({
      content: 'Espera por favor...',
      spinner: 'bubbles'
    });

    loading.present();
    this.restaurantsService.deleteFavourite( JSON.stringify( data ) ).subscribe(
      res => {
        let restaurantsData = res;

        for ( let restaurant of restaurantsData) {

          for ( let product of restaurant.products ) {

            product['removeButtonStyle'] = false;
            product['addButtonStyle'] = false;

            if ( product.active ) {
              switch( product.category ) {
                case 'Entradas': {
                  restaurant.dayMenu.starters.push(product);
                  break;
                }
                case 'Intermedios': {
                  restaurant.dayMenu.midDish.push(product);
                  break;
                }
                case 'Plato Fuerte': {
                  restaurant.dayMenu.mainDish.push(product);
                  break;
                }
                case 'Bebidas': {
                  restaurant.dayMenu.drinks.push(product);
                  break;
                }
                case 'Postres': {
                  restaurant.dayMenu.desserts.push(product);
                  break;
                }
              }
            }

          }

        }

        this.restaurants = restaurantsData;
        loading.dismiss();
      },
      error => {
        console.log(error);
      }
    );
  }

}
